<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%url_group}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%url}}`
 * - `{{%permission}}`
 */
class m190526_144018_create_url_group_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%url_group}}', [
            'url_id' => $this->integer(),
            'permission_id' => $this->integer(),
        ]);

        $this->addPrimaryKey('url_group_pk', 'url_group', ['url_id', 'permission_id']);

        // creates index for column `url_id`
        $this->createIndex(
            '{{%idx-url_group-url_id}}',
            '{{%url_group}}',
            'url_id'
        );

        // add foreign key for table `{{%url}}`
        $this->addForeignKey(
            '{{%fk-url_group-url_id}}',
            '{{%url_group}}',
            'url_id',
            '{{%url}}',
            'id',
            'CASCADE'
        );

        // creates index for column `permission_id`
        $this->createIndex(
            '{{%idx-url_group-permission_id}}',
            '{{%url_group}}',
            'permission_id'
        );

        // add foreign key for table `{{%permission}}`
        $this->addForeignKey(
            '{{%fk-url_group-permission_id}}',
            '{{%url_group}}',
            'permission_id',
            '{{%permission}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%url}}`
        $this->dropForeignKey(
            '{{%fk-url_group-url_id}}',
            '{{%url_group}}'
        );

        // drops index for column `url_id`
        $this->dropIndex(
            '{{%idx-url_group-url_id}}',
            '{{%url_group}}'
        );

        // drops foreign key for table `{{%permission}}`
        $this->dropForeignKey(
            '{{%fk-url_group-permission_id}}',
            '{{%url_group}}'
        );

        // drops index for column `permission_id`
        $this->dropIndex(
            '{{%idx-url_group-permission_id}}',
            '{{%url_group}}'
        );

        $this->dropTable('{{%url_group}}');
    }
}
