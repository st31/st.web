# ST V.3.1.0 (web) 

## The hierarchy of partitions

* **Testing in the disciplines**
    * CRUD of tests and questions
    * Passing tests by users
* **Administration by users**
    * CRUD of roles
    * CRUD of users
* **Didactic materials**
    * CRUD of didactic materials
* **Access security**
    * Authorization / authentification
    * Sign up
* **Analytics of testing**
    * Analytics in the context of tests
    * Analytics in the context of groups
    * Analytics in the context of tests and groups
    * Analytics in the context of a specific user
* **Other**
    * Clever idea
    * Feedback
    * CRUD of menu
    
## Scheme tree API

* modules 
    * security
        * userController
            * registration(...)
            * auth(...)
            * delete(...)
    * user
        * categoryController
            * get()
            * get($userId)
            * create(...)
            * change($userId)
            * delete(...)
        * roleController
            * getList()
            * get(...)
            * create(...)
            * change(...)
            * delete(...)
        * feedbackController()
            * create(...)
            * send(...)
            * getList()
            * get($idLetter)
            * marked(...)
    * training
        * testController
            * getList()
            * get($testId)
            * create(...)
            * change(...)
            * passing(...)
            * delete(...)
        * analyticsContoller
            * get($testId, $typeOfContext)
        * materialController
            * getList()
            * get($idMaterial)
            * create(...)
            * change(...)
            * delete(...)