<?php

use yii\db\Migration;

/**
 * Class m190528_170558_update_column_UUID_to_user_table
 */
class m190528_170558_update_column_UUID_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('user','UUID','string(36)');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('user','UUID','tinyint AFTER `email`');
    }
}
