<?php

namespace app\modules\security\models;

use Yii;

/**
 * This is the model class for table "url".
 *
 * @property int $id
 * @property string $path
 *
 * @property UrlGroup[] $urlGroups
 * @property Permission[] $permissions
 */
class Url extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'url';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['path'], 'required'],
            [['path'], 'string', 'max' => 255],
            [['path'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'path' => 'Path',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUrlGroups()
    {
        return $this->hasMany(UrlGroup::className(), ['url_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPermissions()
    {
        return $this->hasMany(Permission::className(), ['id' => 'permission_id'])->viaTable('url_group', ['url_id' => 'id']);
    }
}
