<?php

namespace app\modules\content\controllers;

use yii\rest\ActiveController;
use yii\web\Response;


class MaterialController extends ActiveController
{
    public $modelClass = 'app\modules\user\models\User';

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => [],
                'formats' => ['application/json' => Response::FORMAT_JSON],
            ],
        ];
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['index']);
        unset($actions['view']);
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);
        unset($actions['options']);

        return $actions;
    }

    /**
     * @return string
     */
    public function actionDefault()
    {
        return json_encode(['error' => 'Method default']);
    }

    /**
     * @return false|string
     */
    public function actionAdd()
    {
        return json_encode(['data' => 'Method "add"']);
    }

    /**
     * @return false|string
     */
    public function actionRemove()
    {
        return json_encode(['data' => 'Method "remove"']);
    }

    /**
     * @return false|string
     */
    public function actionRead()
    {
        return json_encode(['data' => 'Method "read"']);
    }

    /**
     * @return false|string
     */
    public function actionEdit()
    {
        return json_encode(['data' => 'Method "edit"']);
    }
}
