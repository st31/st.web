<?php

namespace app\modules\test\controllers;

use yii\rest\ActiveController;
use yii\web\Response;


class GeneralController extends ActiveController
{
    public $modelClass = 'app\modules\user\models\User';

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['index', 'view', 'create', 'update', 'search'],
                'formats' => ['application/json' => Response::FORMAT_JSON],
            ],
        ];
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['index']);
        unset($actions['view']);
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);
        unset($actions['options']);
        return $actions;
    }

    /**
     * @return string
     */
    public function actionDefault()
    {
        return json_encode(['error' => 'Method default']);
    }

    /**
     * @return false|string
     */
    public function actionPassed()
    {
        return json_encode(['error' => 'general Method "passed"']);
    }

    /**
     * @return false|string
     */
    public function actionAdd()
    {
        return json_encode(['error' => 'general Method "add"']);
    }

    /**
     * @return false|string
     */
    public function actionRemove()
    {
        return json_encode(['error' => 'general Method "remove"']);
    }

    /**
     * @return false|string
     */
    public function actionEdit()
    {
        return json_encode(['error' => 'general Method "edit"']);
    }
}
