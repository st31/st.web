<?php

namespace app\modules\security\controllers;

use app\modules\security\models\Role;
use app\modules\security\services\AccessService;
use app\modules\security\services\MessageService;
use Throwable;
use Yii;
use yii\db\StaleObjectException;
use yii\rest\ActiveController;
use yii\web\Response;


class RoleController extends ActiveController
{
    public $modelClass = 'app\modules\user\models\User';

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['index', 'view', 'create', 'update', 'search'],
                'formats' => ['application/json' => Response::FORMAT_JSON],
            ],
        ];
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['index']);
        unset($actions['view']);
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);
        unset($actions['options']);

        return $actions;
    }

    /**
     * @return string
     */
    public function actionDefault()
    {
        return json_encode(['error' => 'Method default']);
    }

    /**
     * @return array
     */
    public function actionAdd()
    {
        $userStatus = AccessService::getStatusUser();
        if ($userStatus != null) {
            return json_encode($userStatus);
        }

        $result = [];
        $model = new Role();

        if ($model->load(Yii::$app->request->post(), '') && $model->validate()) {
            $model->created_at = time();

            if ($model->save()) {
                $result['result'] = ['isSuccess' => TRUE];
            } else {
                $result['error'] = $model->errors;
            }
        } else {
            $result['error'] = $model->errors;
        }

        return json_encode($result);
    }

    /**
     * @return array
     */
    public function actionRemove()
    {
        $userStatus = AccessService::getStatusUser();
        if ($userStatus != null) {
            return json_encode($userStatus);
        }

        $roleId = intval(Yii::$app->request->post()['role_id']);
        $url = Role::findOne($roleId);

        $result = [];
        if (isset($url)) {
            try {
                $resultDelete = $url->delete();
                if ($resultDelete) {
                    $result['result'] = MessageService::messageRoleIsRemovedSuccessfully();
                } else {
                    $result['result'] = MessageService::messageRoleIsRemovedFail();
                }
            } catch (StaleObjectException $e) {
                $result['error'] = $e->getMessage();
            } catch (Throwable $e) {
                $result['error'] = $e->getMessage();
            }
        } else {
            $result['error'] = MessageService::messageRoleNotFound();
        }

        return json_encode($result);
    }

    /**
     * @return array
     */
    public function actionEdit()
    {
        $userStatus = AccessService::getStatusUser();
        if ($userStatus != null) {
            return json_encode($userStatus);
        }

        $roleId = intval(Yii::$app->request->post()['role_id']);
        $modelRole = Role::findOne($roleId);
        if (!isset($modelRole)) {
            return ['error' => ['role' => MessageService::messageRoleNotFound()]];
        }

        $saveEditRole = $this->saveEditRole($modelRole);
        $result = $saveEditRole['result'];

        if (!$saveEditRole['resultModelSave']) {
            $result['error']['permission'] = MessageService::messageRoleIsEditFail();

            return json_encode($result);
        }

        return json_encode($result);
    }

    /**
     * @param $modelRole Role
     *
     * @return array
     */
    private function saveEditRole($modelRole)
    {
        $result = [];
        $resultModelSave = false;

        if ($modelRole->load(Yii::$app->request->post()['data'], '') && $modelRole->validate()) {
            $modelRole->updated_at = time();

            $resultModelSave = $modelRole->save();
            if ($resultModelSave) {
                $result['result']['role'] = MessageService::messageRoleIsEditSuccessfully();
            } else {
                $result['error']['role'] = MessageService::messageRoleIsEditFail();
            }
        }

        return [
            'result' => $result,
            'resultModelSave' => $resultModelSave
        ];
    }
}
