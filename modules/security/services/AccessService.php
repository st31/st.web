<?php

namespace app\modules\security\services;


use app\modules\security\models\PermissionGroup;
use app\modules\security\models\Role;
use app\modules\security\models\Url;
use app\modules\security\models\UrlGroup;
use app\modules\user\models\User;
use Yii;

/**
 * Class AccessService
 *
 * @package app\modules\security\services
 */
class AccessService
{
    /**
     * @return array|bool|null
     */
    public static function getStatusUser()
    {
        $userStatus = AccessService::checkStatusUser();
        $userStatusCode = $userStatus['code'];

        if ($userStatusCode == 1 || $userStatusCode == -1) {
            return null;
        } else {
            return ['error' => $userStatus];
        }
    }

    /**
     * @param $action string
     *
     * @return bool
     */
    public static function checkAccess($action)
    {
        $session = AccessService::openSession();
        $UUID = $session->get('UUID');

        if (!AccessService::checkAuth() && !AccessService::checkUserIntoDB($UUID)) {
            $session->close();

            return false;
        }

        /** @var User $currentUser */
        $currentUser = User::find()->where(['UUID' => $UUID])->one();
        if (!isset($currentUser)) {
            return false;
        }

        $urlPaths = AccessService::extractionPathsUrl($currentUser->role_id);

        $result = false;

        /** @var array<string> $urlPaths */
        foreach ($urlPaths as $urlPath) {
            if ($urlPath == $action) {
                $result = true;
                break;
            }
        }

        $session->close();

        return $result;
    }

    /**
     * @return array
     */
    private static function checkStatusUser()
    {
        $session = AccessService::openSession();
        $UUID = $session->get('UUID');

        /** @var User $user */
        $user = User::find()->where(['UUID' => $UUID])->one();
        if (!isset($user)) {
            $session->close();
            return [
                'code' => -1,
                'message' => 'User not found',
            ];
        }

        $result = '';
        switch ($user->status) {
            case 0:
                $result = [
                    'code' => $user->status,
                    'message' => MessageService::messageAccountNotActivated(),
                ];
                break;
            case 1:
                $result = [
                    'code' => $user->status,
                    'message' => MessageService::messageAccessAllowed(),
                ];
                break;
            case 2:
                $result = [
                    'code' => $user->status,
                    'message' => MessageService::messageAccountBlocked(),
                ];
                break;
            default:
                $result = [
                    'code' => null,
                    'message' => MessageService::messageFailIdentificationUserStatus(),
                ];
        }

        $session->close();

        return $result;
    }

    /**
     * @param $role_id integer
     *
     * @return array|bool
     */
    private static function extractionPathsUrl($role_id)
    {
        /** @var Role $userRole */
        $userRoleId = $role_id;

        /** @var PermissionGroup[] $permissionGroups */
        $permissionGroups = PermissionGroup::find()->where(['role_id' => $userRoleId])->all();
        if (AccessService::checkNotCorrectArray($permissionGroups)) {
            return [];
        }

        /** @var array<string> $permissionIds */
        $permissionIds = AccessService::collectionPermissionIds($permissionGroups);
        if (AccessService::checkNotCorrectArray($permissionIds)) {
            return [];
        }

        /** @var array<integer> $urlGroups */
        $urlIds = AccessService::collectionUrlIds($permissionIds);
        if (AccessService::checkNotCorrectArray($urlIds)) {
            return [];
        }

        $urlPaths = AccessService::collectionUrlPaths($urlIds);

        return $urlPaths;
    }

    /**
     * @param $value array
     *
     * @return bool
     */
    private static function checkNotCorrectArray($value)
    {
        return !is_array($value) || count($value) == 0;
    }

    /**
     * @param $urlIds array<integer>
     *
     * @return array<string>
     */
    private static function collectionUrlPaths($urlIds)
    {
        $urlPaths = [];
        $urlsQuery = Url::find();

        /** @var array<string> $urlIds */
        foreach ($urlIds as $urlId) {
            $urlsQuery->orWhere(['id' => $urlId]);
        }
        $urls = $urlsQuery->all();

        /** @var Url[] $urls */
        foreach ($urls as $url) {
            array_push($urlPaths, $url->path);
        }

        return $urlPaths;
    }

    /**
     * @param $permissionGroups PermissionGroup[]
     *
     * @return array
     */
    private static function collectionPermissionIds($permissionGroups)
    {
        $permissionIds = [];
        foreach ($permissionGroups as $permissionGroup) {
            array_push($permissionIds, $permissionGroup->permission_id);
        }

        return $permissionIds;
    }

    /**
     * @param $permissionIds array<string>
     *
     * @return array<integer>
     */
    private static function collectionUrlIds($permissionIds)
    {
        if (!is_array($permissionIds) && count($permissionIds) == 0) {
            return [];
        }

        $urlIds = [];
        $urlGroupsQuery = UrlGroup::find();

        /** @var string $permissionId */
        foreach ($permissionIds as $permissionId) {
            $urlGroupsQuery->orWhere(['permission_id' => $permissionId]);
        }
        $urlGroups = $urlGroupsQuery->all();

        /** @var UrlGroup[] $urlGroups */
        foreach ($urlGroups as $urlGroup) {
            array_push($urlIds, $urlGroup->url_id);
        }

        return $urlIds;
    }

    /**
     * @return bool
     */
    private static function checkAuth()
    {
        $session = AccessService::openSession();

        $result = $session->has('UUID');

        $session->close();

        return $result;
    }

    /**
     * @param $UUID string
     *
     * @return bool
     */
    private static function checkUserIntoDB($UUID)
    {
        $user = User::find()->where(['UUID' => $UUID])->one();

        return isset($user);
    }

    /**
     * @return mixed|yii\web\Session
     */
    private static function openSession()
    {
        $session = Yii::$app->session;
        if (!$session->isActive) {
            $session->open();
        }

        return $session;
    }
}