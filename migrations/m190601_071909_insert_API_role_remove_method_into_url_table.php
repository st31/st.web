<?php

use yii\db\Migration;

/**
 * Class m190601_071909_insert_API_role_remove_method_into_url_table
 */
class m190601_071909_insert_API_role_remove_method_into_url_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('url', [
            'path' => '/api/security/role/remove'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('url', [
            'path' => '/api/security/role/remove'
        ]);
    }
}
