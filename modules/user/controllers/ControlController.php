<?php

namespace app\modules\user\controllers;

use yii\rest\ActiveController;
use yii\web\Response;


class ControlController extends ActiveController
{
    public $modelClass = 'app\modules\user\models\User';

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['index', 'view', 'create', 'update', 'search'],
                'formats' => ['application/json' => Response::FORMAT_JSON],
            ],
        ];
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['index']);
        unset($actions['view']);
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);
        unset($actions['options']);

        return $actions;
    }

    /**
     * @return string
     */
    public function actionDefault()
    {
        return json_encode(['error' => 'Method default']);
    }

    /**
     * @return false|string
     */
    public function actionAdd()
    {
        return json_encode(['error' => 'Method "add"']);
    }

    /**
     * @return false|string
     */
    public function actionRemove()
    {
        return json_encode(['error' => 'Method "remove"']);
    }

    /**
     * @return false|string
     */
    public function actionEdit()
    {
        return json_encode(['error' => 'Method "edit"']);
    }

    /**
     * @return false|string
     */
    public function actionProfile()
    {
        return json_encode(['error' => 'Method "profile"']);
    }

    /**
     * @return false|string
     */
    public function actionEditPassword()
    {
        return json_encode(['error' => 'Method "edit password"']);
    }

    /**
     * @return false|string
     */
    public function actionRecoveryPassword()
    {
        return json_encode(['error' => 'Method "recovery password"']);
    }
}
