<?php

namespace app\modules\user\models;

use app\modules\security\models\Role;


/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $login
 * @property string $password
 * @property string $name
 * @property string $surname
 * @property string $patronymic
 * @property string $email
 * @property string $UUID
 * @property int $status
 * @property int $role_id
 * @property int $user_category_id
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Role $role
 * @property UserCategory $userCategory
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['login', 'password', 'email'], 'required'],
            [['status', 'role_id', 'user_category_id', 'created_at', 'updated_at'], 'integer'],
            [['login', 'password', 'name', 'surname', 'patronymic', 'email'], 'string', 'max' => 255],
            [['UUID'], 'string', 'max' => 36],
            [['login'], 'unique'],
            [['email'], 'unique'],
            [['email'], 'email'],
            [['role_id'], 'exist', 'skipOnError' => true, 'targetClass' => Role::className(), 'targetAttribute' => ['role_id' => 'id']],
            [['user_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserCategory::className(), 'targetAttribute' => ['user_category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Login',
            'password' => 'Password',
            'name' => 'Name',
            'surname' => 'Surname',
            'patronymic' => 'Patronymic',
            'email' => 'Email',
            'UUID' => 'UUID',
            'status' => 'Status',
            'role_id' => 'Role ID',
            'user_category_id' => 'User Category ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(Role::className(), ['id' => 'role_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCategory()
    {
        return $this->hasOne(UserCategory::className(), ['id' => 'user_category_id']);
    }
}
