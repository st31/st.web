<?php

use yii\db\Migration;

/**
 * Class m190601_170250_add_columns_dates_into_permission_table
 */
class m190601_170250_add_columns_dates_into_permission_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('permission', 'created_at', 'int(11)');
        $this->addColumn('permission', 'updated_at', 'int(11)');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('permission', 'created_at');
        $this->dropColumn('permission', 'updated_at');
    }
}
