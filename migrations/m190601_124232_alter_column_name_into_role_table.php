<?php

use yii\db\Migration;

/**
 * Class m190601_124232_alter_column_name_into_role_table
 */
class m190601_124232_alter_column_name_into_role_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('unique-name-role', 'role', 'name', $unique = true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('unique-name-role', 'role');
    }
}
