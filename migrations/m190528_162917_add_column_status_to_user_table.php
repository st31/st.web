<?php

use yii\db\Migration;

/**
 * Class m190528_162917_add_column_status_to_user_table
 */
class m190528_162917_add_column_status_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'status', 'tinyint AFTER `email`');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'status');
    }
}
