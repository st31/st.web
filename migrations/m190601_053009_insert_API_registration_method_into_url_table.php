<?php

use yii\db\Migration;

/**
 * Class m190601_053009_insert_API_registration_method_into_url_table
 */
class m190601_053009_insert_API_registration_method_into_url_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('url', [
            'path' => '/api/security/access/registration'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('url', [
            'path' => '/api/security/access/registration'
        ]);
    }
}
