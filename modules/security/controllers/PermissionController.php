<?php

namespace app\modules\security\controllers;

use app\modules\security\models\Permission;
use app\modules\security\services\AccessService;
use app\modules\security\services\MessageService;
use Yii;
use yii\db\StaleObjectException;
use yii\rest\ActiveController;
use yii\web\Response;


class PermissionController extends ActiveController
{
    public $modelClass = 'app\modules\user\models\User';

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => [],
                'formats' => ['application/json' => Response::FORMAT_JSON],
            ],
        ];
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['index']);
        unset($actions['view']);
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);
        unset($actions['options']);

        return $actions;
    }

    /**
     * @return string
     */
    public function actionDefault()
    {
        return json_encode(['error' => 'Method default']);
    }

    /**
     * @return array
     */
    public function actionAdd()
    {
        $userStatus = AccessService::getStatusUser();
        if ($userStatus != null) {
            return json_encode($userStatus);
        }

        $result = [];
        $model = new Permission();
        if ($model->load(Yii::$app->request->post(), '') && $model->validate()) {
            $model->created_at = time();

            if ($model->save()) {
                $result['result'] = MessageService::messagePermissionCreateSuccessfully();
            } else {
                $result['error'] = $model->errors;
            }
        } else {
            $result['error'] = $model->errors;
        }

        return json_encode($result);
    }

    /**
     * @return array
     */
    public function actionRemove()
    {
        $userStatus = AccessService::getStatusUser();
        if ($userStatus != null) {
            return json_encode($userStatus);
        }

        $permissionId = $this->getPermissionId();
        $model = Permission::findOne($permissionId);

        if (!isset($model)) {
            return ['error' => MessageService::messagePermissionNotFound()];
        }

        $result = [];
        try {
            $model->delete();
            $result['result'] = MessageService::messagePermissionRemoveSuccess();
        } catch (StaleObjectException $e) {
            $result['error'] = $e->getMessage();
        } catch (\Throwable $e) {
            $result['error'] = $e->getMessage();
        }

        return json_encode($result);
    }

    /**
     * @return array
     */
    public function actionEdit()
    {
        $userStatus = AccessService::getStatusUser();
        if ($userStatus != null) {
            return json_encode($userStatus);
        }

        $permissionId = $this->getPermissionId();
        $model = Permission::findOne($permissionId);

        if (!isset($model)) {
            return ['error' => MessageService::messagePermissionNotFound()];
        }

        if ($model->load(Yii::$app->request->post()['data'], '') && $model->validate()) {
            $model->updated_at = time();

            if ($model->save()) {
                $result['result'] = MessageService::messagePermissionEditSuccessfully();
            } else {
                $result['error'] = $model->errors;
            }
        } else {
            $result['error'] = MessageService::messagePermissionEditFail();
        }

        return json_encode($result);
    }

    /**
     * @return int
     */
    private function getPermissionId()
    {
        return intval(Yii::$app->request->post()['permission_id']);
    }
}
