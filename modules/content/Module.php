<?php

namespace app\modules\content;

use Yii;

/**
 * content module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\content\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    public function beforeAction($action)
    {

        return true; // false - Forbidden


//        if (!parent::beforeAction($action)) {
//            return false;
//        }
//
//        if (!Yii::$app->user->isGuest) {
//            return true;
//        } else {
//            Yii::$app->getResponse()->redirect(Yii::$app->getHomeUrl());
//            //для перестраховки вернем false
//            return false;
//        }
    }
}
