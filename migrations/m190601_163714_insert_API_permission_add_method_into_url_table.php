<?php

use yii\db\Migration;

/**
 * Class m190601_163714_insert_API_permission_add_method_into_url_table
 */
class m190601_163714_insert_API_permission_add_method_into_url_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('url', [
            'path' => '/api/security/permission/add'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('url', [
            'path' => '/api/security/permission/add'
        ]);
    }
}
