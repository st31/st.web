<?php

namespace app\modules\security;

use app\modules\security\services\AccessService;

/**
 * security module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\security\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {


        parent::init();
    }

    public function beforeAction($action)
    {


        if ($action->id == 'auth') {
            return true;
        }

        return AccessService::checkAccess($_SERVER['REQUEST_URI']);
    }
}
