<?php

namespace app\modules\statistics\controllers;

use yii\rest\ActiveController;
use yii\web\Response;


class AnalysisController extends ActiveController
{
    public $modelClass = 'app\modules\user\models\User';

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => [],
                'formats' => ['application/json' => Response::FORMAT_JSON],
            ],
        ];
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['index']);
        unset($actions['view']);
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);
        unset($actions['options']);

        return $actions;
    }

    /**
     * @return string
     */
    public function actionDefault()
    {
        return json_encode(['error' => 'Method default']);
    }

    public function actionContextGroup()
    {
        return json_encode(['error' => 'Method "context-group"']);
    }

    public function actionContextGroups()
    {
        return json_encode(['error' => 'Method "context-groups"']);
    }

    public function actionPersonalRating()
    {
        return json_encode(['error' => 'Method "personal-rating"']);
    }
}
