<?php

use yii\db\Migration;

/**
 * Class m190528_170002_add_column_UUID_to_user_table
 */
class m190528_170002_add_column_UUID_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'UUID', 'tinyint AFTER `email`');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'UUID');
    }
}
