<?php


namespace app\modules\security\services;


class MessageService
{
    /**
     * @return array
     */
    public static function messageLoginOrPasswordNotCorrect()
    {
        return MessageService::createMessage(1, 'Login or password not correct');
    }

    /**
     * @return string
     */
    public static function messageAccountNotActivated()
    {
        return 'Account not activated';
    }

    /**
     * @return string
     */
    public static function messageFailIdentificationUserStatus()
    {
        return 'Fail identification user status';
    }

    /**
     * @return string
     */
    public static function messageAccountBlocked()
    {
        return 'Account blocked';
    }

    /**
     * @return array
     */
    public static function messageAccessAllowed()
    {
        return MessageService::createMessage(2, 'Access allowed');
    }

    /**
     * @return string
     */
    public static function messageSavingNotComplete()
    {
        return 'Saving not complete';
    }

    /**
     * @return string
     */
    public static function messageRoleIsRemovedSuccessfully()
    {
        return 'The role is removed successfully';
    }

    /**
     * @return string
     */
    public static function messageRoleIsRemovedFail()
    {
        return 'The role is removed fail';
    }

    /**
     * @return string
     */
    public static function messageRoleIsEditSuccessfully()
    {
        return 'The role is edit successfully';
    }

    /**
     * @return string
     */
    public static function messageRoleIsEditFail()
    {
        return 'The role is edit fail';
    }

    /**
     * @return string
     */
    public static function messageRoleNotFound()
    {
        return 'The role not found';
    }

    /**
     * @return string
     */
    public static function messagePermissionGroupNotFound()
    {
        return 'The permission group not found';
    }

    /**
     * @return string
     */
    public static function messagePermissionCreateSuccessfully()
    {
        return 'The permission create successfully';
    }

    /**
     * @return string
     */
    public static function messagePermissionNotFound()
    {
        return 'The permission not found';
    }

    /**
     * @return string
     */
    public static function messagePermissionRemoveSuccess()
    {
        return 'The permission remove success';
    }

    /**
     * @return string
     */
    public static function messagePermissionRemoveFail()
    {
        return 'The permission remove fail';
    }

    /**
     * @return string
     */
    public static function messagePermissionEditSuccessfully()
    {
        return 'The permission edit successfully';
    }

    /**
     * @return string
     */
    public static function messagePermissionEditFail()
    {
        return 'The permission edit fail';
    }


    /**
     * @param $code integer
     * @param $message string
     * @param $additionalInfo object
     *
     * @return array
     */
    private static function createMessage($code, $message, $additionalInfo = null)
    {
        $result = [
            'code' => $code,
            'message' => $message,
        ];

        if ($additionalInfo != null) {
            $result['additional_info'] = $additionalInfo;
        }

        return $result;
    }
}