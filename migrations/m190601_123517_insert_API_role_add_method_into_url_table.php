<?php

use yii\db\Migration;

/**
 * Class m190601_123517_insert_API_role_add_method_into_url_table
 */
class m190601_123517_insert_API_role_add_method_into_url_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('url', [
            'path' => '/api/security/role/add'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('url', [
            'path' => '/api/security/role/add'
        ]);
    }
}
