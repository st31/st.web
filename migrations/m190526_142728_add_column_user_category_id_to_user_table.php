<?php

use yii\db\Migration;

/**
 * Class m190526_142728_add_column_user_category_id_to_user_table
 */
class m190526_142728_add_column_user_category_id_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'user_category_id', 'integer AFTER `email` ');

        $this->createIndex(
            '{{%idx-user-user_category_id}}',
            '{{%user}}',
            'user_category_id'
        );

        $this->addForeignKey(
            '{{%fk-user-user_category_id}}',
            '{{%user}}',
            'user_category_id',
            '{{%user_category}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            '{{%fk-user-user_category_id}}',
            '{{%user}}'
        );

        $this->dropIndex(
            '{{%idx-user-user_category_id}}',
            '{{%user}}'
        );

        $this->dropColumn('user', 'user_category_id');
    }
}
