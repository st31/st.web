<?php

use yii\db\Migration;

/**
 * Class m190601_173018_insert_API_permission_edit_method_into_url_table
 */
class m190601_173018_insert_API_permission_edit_method_into_url_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('url', [
            'path' => '/api/security/permission/edit'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('url', [
            'path' => '/api/security/permission/edit'
        ]);
    }
}
