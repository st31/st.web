<?php

namespace app\modules\security\models;

use Yii;

/**
 * This is the model class for table "permission".
 *
 * @property int $id
 * @property string $name
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property PermissionGroup[] $permissionGroups
 * @property Role[] $roles
 * @property UrlGroup[] $urlGroups
 * @property Url[] $urls
 */
class Permission extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'permission';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPermissionGroups()
    {
        return $this->hasMany(PermissionGroup::className(), ['permission_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoles()
    {
        return $this->hasMany(Role::className(), ['id' => 'role_id'])->viaTable('permission_group', ['permission_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUrlGroups()
    {
        return $this->hasMany(UrlGroup::className(), ['permission_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUrls()
    {
        return $this->hasMany(Url::className(), ['id' => 'url_id'])->viaTable('url_group', ['permission_id' => 'id']);
    }
}
