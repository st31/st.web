<?php

namespace app\modules\security\controllers;

use app\modules\security\services\AccessService;
use app\modules\security\services\MessageService;
use app\modules\user\models\User;
use Faker\Provider\Uuid;
use Yii;
use yii\rest\ActiveController;
use yii\web\Response;


class AccessController extends ActiveController
{
    /**
     * @var string
     */
    public $modelClass = 'app\modules\user\models\User';

    public static function allowedDomains()
    {
        return [
            'http://localhost:4200',
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
//        $behaviors = parent::behaviors();
//
//        // remove authentication filter
//        $auth = $behaviors['authenticator'];
//        unset($behaviors['authenticator']);
//
//        // add CORS filter
//        $behaviors['corsFilter'] = [
//            'class' => \yii\filters\Cors::className(),
//        ];
//
//        // re-add authentication filter
//        $behaviors['authenticator'] = $auth;
//        // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)
//        $behaviors['authenticator']['except'] = ['options'];
//
//        return $behaviors;

        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => [],
                'formats' => ['application/json' => Response::FORMAT_JSON],
            ],
        ];
    }

    /**
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();

        unset($actions['index']);
        unset($actions['view']);
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);
        unset($actions['options']);

        return $actions;
    }

    /**
     * @return string
     */
    public function actionDefault()
    {
        return json_encode(['data' => 'Method default']);
    }

    /**
     * @return false|string
     *
     * @throws Yii\base\Exception
     */
    public function actionRegistration()
    {
        $userStatus = AccessService::getStatusUser();
        if ($userStatus != null) {
            return json_encode($userStatus);
        }

        $result = null;

        $model = new User();
        if ($model->load(Yii::$app->request->post(), '') && $model->validate()) {
            $model = $this->createAdditionalData($model);

            if ($model->save()) {
                $result = [
                    'result' => [
                        'is_registration' => TRUE,
                        'login' => $model->login,
                    ]
                ];
            } else {
                $result = ['error' => $model->errors];
            }
        } else {
            $result = ['error' => $model->errors];
        }

        return json_encode($result);
    }

    /**
     * @return array
     */
    public function actionAuth()
    {
        $userStatus = AccessService::getStatusUser();
        if ($userStatus != null) {
            return $userStatus;
        }

        $login = Yii::$app->request->post()['login'];
        $password = Yii::$app->request->post()['password'];
        if (is_null($login) || is_null($password)) {
            $response_error['result'] = MessageService::messageLoginOrPasswordNotCorrect();
            return json_encode($response_error);
        }

        /** @var User $model */
        $model = User::find()->where('login = :login', [':login' => $login])->one();
        if (is_null($model)) {
            $response_error['result'] = MessageService::messageLoginOrPasswordNotCorrect();
            return $response_error;
        }

        $response = [];
        if ($model->status == 1) {
            $response = $this->savingAuthData($password, $model);
        } elseif ($model->status == 0) {
            $this->removeUuidFromSession();
            $response['result'] = MessageService::messageAccountNotActivated();
        } elseif ($model->status == 2) {
            $this->removeUuidFromSession();
            $response['result'] = MessageService::messageAccountBlocked();
        }

        return $response;
    }

    /**
     * @return void
     */
    private function removeUuidFromSession()
    {
        $session = Yii::$app->session;
        if (!$session->isActive) {
            $session->open();
        }

        if ($session->has('UUID')) {
            $session->remove('UUID');
        }

        if ($session->isActive) {
            $session->close();
        }
    }

    /**
     * @param $password string
     * @param $model User
     *
     * @return array
     */
    private function savingAuthData($password, $model)
    {
        $result = [];

        if (Yii::$app->getSecurity()->validatePassword($password, $model->password)) {
            $session = Yii::$app->session;
            if (!$session->isActive) {
                $session->open();
            }

            $UUID = Uuid::uuid();
            $model->UUID = $UUID;
            $model->updated_at = time();

            if ($model->save()) {
                $session->set('UUID', $UUID);
                $result['result'] = MessageService::messageAccessAllowed();
            } else {
                $result['result'] = MessageService::messageSavingNotComplete();
            }

            $session->close();
        } else {
            $result['result'] = MessageService::messageLoginOrPasswordNotCorrect();
        }

        return $result;
    }

    /**
     * @param $model User
     *
     * @return User
     *
     * @throws yii\base\Exception
     */
    private function createAdditionalData($model)
    {
        $model->password = Yii::$app->getSecurity()->generatePasswordHash($model->password);
        $model->created_at = time();
        $model->status = 0; // account not activate

        return $model;
    }
}
