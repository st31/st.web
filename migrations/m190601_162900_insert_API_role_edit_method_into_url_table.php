<?php

use yii\db\Migration;

/**
 * Class m190601_162900_insert_API_role_edit_method_into_url_table
 */
class m190601_162900_insert_API_role_edit_method_into_url_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('url', [
            'path' => '/api/security/role/edit'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('url', [
            'path' => '/api/security/role/edit'
        ]);
    }
}
