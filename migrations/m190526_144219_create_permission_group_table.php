<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%permission_group}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%permission}}`
 * - `{{%role}}`
 */
class m190526_144219_create_permission_group_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%permission_group}}', [
            'permission_id' => $this->integer(),
            'role_id' => $this->integer(),
        ]);

        $this->addPrimaryKey('permission_group_pk', 'permission_group', ['permission_id', 'role_id']);

        // creates index for column `permission_id`
        $this->createIndex(
            '{{%idx-permission_group-permission_id}}',
            '{{%permission_group}}',
            'permission_id'
        );

        // add foreign key for table `{{%permission}}`
        $this->addForeignKey(
            '{{%fk-permission_group-permission_id}}',
            '{{%permission_group}}',
            'permission_id',
            '{{%permission}}',
            'id',
            'CASCADE'
        );

        // creates index for column `role_id`
        $this->createIndex(
            '{{%idx-permission_group-role_id}}',
            '{{%permission_group}}',
            'role_id'
        );

        // add foreign key for table `{{%role}}`
        $this->addForeignKey(
            '{{%fk-permission_group-role_id}}',
            '{{%permission_group}}',
            'role_id',
            '{{%role}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%permission}}`
        $this->dropForeignKey(
            '{{%fk-permission_group-permission_id}}',
            '{{%permission_group}}'
        );

        // drops index for column `permission_id`
        $this->dropIndex(
            '{{%idx-permission_group-permission_id}}',
            '{{%permission_group}}'
        );

        // drops foreign key for table `{{%role}}`
        $this->dropForeignKey(
            '{{%fk-permission_group-role_id}}',
            '{{%permission_group}}'
        );

        // drops index for column `role_id`
        $this->dropIndex(
            '{{%idx-permission_group-role_id}}',
            '{{%permission_group}}'
        );

        $this->dropTable('{{%permission_group}}');
    }
}
