<?php

use yii\db\Migration;

/**
 * Class m190528_165038_update_column_password_to_user_table
 */
class m190528_165038_update_column_password_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('user','password','string(255) NOT NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('user','password','string(32) NOT NULL');
    }
}
